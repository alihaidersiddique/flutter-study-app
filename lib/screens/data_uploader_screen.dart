import 'package:flutter/material.dart';
import 'package:flutter_study_app/controllers/question_papers/data_uploader_controller.dart';
import 'package:flutter_study_app/firebase_ref/loading_status.dart';
import 'package:get/get.dart';

class DataUploaderScreen extends GetView<DataUploaderController> {
  DataUploaderScreen({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      body: Center(
        child: Obx(
          () => Text(
            controller.loadingStatus.value == LoadingStatus.completed
                ? "Uploading completed"
                : "Uploading...",
          ),
        ),
      ),
    );
  }
}
