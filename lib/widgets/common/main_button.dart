import 'package:flutter/material.dart';
import 'package:flutter_study_app/configs/themes/app_colors.dart';
import 'package:get/get.dart';

class MainButton extends StatelessWidget {
  final String title;
  final VoidCallback onTap;
  final enabled;
  final Widget? child;
  final Color? color;

  const MainButton({
    Key? key,
    this.title = '',
    required this.onTap,
    this.enabled = true,
    this.child,
    this.color,
  }) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Material(
      type: MaterialType.transparency,
      child: SizedBox(
        height: 55,
        child: InkWell(
          onTap: enabled == false ? null : onTap,
          child: Container(
            width: double.maxFinite,
            padding: const EdgeInsets.all(10),
            decoration: BoxDecoration(
              borderRadius: BorderRadius.circular(15),
              color: color ?? Get.theme.cardColor,
            ),
            child: child ??
                Center(
                  child: Text(
                    title,
                    style: const TextStyle(
                      color: onSurfaceTextColor,
                      fontWeight: FontWeight.bold,
                    ),
                  ),
                ),
          ),
        ),
      ),
    );
  }
}
