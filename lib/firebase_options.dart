// File generated by FlutterFire CLI.
// ignore_for_file: lines_longer_than_80_chars, avoid_classes_with_only_static_members
import 'package:firebase_core/firebase_core.dart' show FirebaseOptions;
import 'package:flutter/foundation.dart'
    show defaultTargetPlatform, kIsWeb, TargetPlatform;

/// Default [FirebaseOptions] for use with your Firebase apps.
///
/// Example:
/// ```dart
/// import 'firebase_options.dart';
/// // ...
/// await Firebase.initializeApp(
///   options: DefaultFirebaseOptions.currentPlatform,
/// );
/// ```
class DefaultFirebaseOptions {
  static FirebaseOptions get currentPlatform {
    if (kIsWeb) {
      return web;
    }
    switch (defaultTargetPlatform) {
      case TargetPlatform.android:
        return android;
      case TargetPlatform.iOS:
        return ios;
      case TargetPlatform.macOS:
        return macos;
      case TargetPlatform.windows:
        throw UnsupportedError(
          'DefaultFirebaseOptions have not been configured for windows - '
          'you can reconfigure this by running the FlutterFire CLI again.',
        );
      case TargetPlatform.linux:
        throw UnsupportedError(
          'DefaultFirebaseOptions have not been configured for linux - '
          'you can reconfigure this by running the FlutterFire CLI again.',
        );
      default:
        throw UnsupportedError(
          'DefaultFirebaseOptions are not supported for this platform.',
        );
    }
  }

  static const FirebaseOptions web = FirebaseOptions(
    apiKey: 'AIzaSyDPz5dE0P27Jvc8wSyFRDp6V6zIXgFCg9o',
    appId: '1:660436444317:web:044433ae1b5fadcfd86074',
    messagingSenderId: '660436444317',
    projectId: 'flutter-study-app-dc1f1',
    authDomain: 'flutter-study-app-dc1f1.firebaseapp.com',
    storageBucket: 'flutter-study-app-dc1f1.appspot.com',
  );

  static const FirebaseOptions android = FirebaseOptions(
    apiKey: 'AIzaSyA69KRoy04Di2CdGrV6VKXjTKfDDIuCsoc',
    appId: '1:660436444317:android:0ddd9389089989c9d86074',
    messagingSenderId: '660436444317',
    projectId: 'flutter-study-app-dc1f1',
    storageBucket: 'flutter-study-app-dc1f1.appspot.com',
  );

  static const FirebaseOptions ios = FirebaseOptions(
    apiKey: 'AIzaSyATd9hnbsZLN_7tvczRlcJbLUn8sT6mhTw',
    appId: '1:660436444317:ios:5c16210be059eb65d86074',
    messagingSenderId: '660436444317',
    projectId: 'flutter-study-app-dc1f1',
    storageBucket: 'flutter-study-app-dc1f1.appspot.com',
    iosClientId: '660436444317-tbpvpnntvpga1pl25qtca4ej1gdirapj.apps.googleusercontent.com',
    iosBundleId: 'com.example.flutterStudyApp',
  );

  static const FirebaseOptions macos = FirebaseOptions(
    apiKey: 'AIzaSyATd9hnbsZLN_7tvczRlcJbLUn8sT6mhTw',
    appId: '1:660436444317:ios:5c16210be059eb65d86074',
    messagingSenderId: '660436444317',
    projectId: 'flutter-study-app-dc1f1',
    storageBucket: 'flutter-study-app-dc1f1.appspot.com',
    iosClientId: '660436444317-tbpvpnntvpga1pl25qtca4ej1gdirapj.apps.googleusercontent.com',
    iosBundleId: 'com.example.flutterStudyApp',
  );
}
