import 'dart:convert';
import 'package:cloud_firestore/cloud_firestore.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/services.dart';
import 'package:flutter_study_app/firebase_ref/loading_status.dart';
import 'package:flutter_study_app/firebase_ref/references.dart';
import 'package:flutter_study_app/models/question_paper_model.dart';
import 'package:get/get.dart';

class DataUploaderController extends GetxController {
  @override
  void onReady() {
    uploadData();
    super.onReady();
  }

  final loadingStatus =
      LoadingStatus.loading.obs; //loading status is observable

  Future<void> uploadData() async {
    loadingStatus.value = LoadingStatus.loading;

    //loading assets root directory
    final manifestContent = await DefaultAssetBundle.of(Get.context!)
        .loadString("AssetManifest.json");

    //assessing all assets paths
    final Map<String, dynamic> manifestMap = json.decode(manifestContent);

    //loading all json files from required path
    final papersInAsset = manifestMap.keys.where((path) {
      return path.startsWith("assets/DB/papers") && path.contains(".json");
    }).toList();

    //converting json into dart object
    List<QuestionPaperModel> questionPapers = [];

    for (var paper in papersInAsset) {
      String stringPaperContent = await rootBundle.loadString(paper);
      questionPapers
          .add(QuestionPaperModel.fromJson(json.decode(stringPaperContent)));
    }

    //creating collections and inserting documents
    var batch = fireStore.batch();

    for (var paper in questionPapers) {
      batch.set(questionPaperRF.doc(paper.id), {
        "title": paper.title,
        "image_url": paper.imageUrl,
        "description": paper.description,
        "time_seconds": paper.timeSeconds,
        "questions_count":
            paper.questions == null ? 0 : paper.questions?.length,
      });

      for (var questions in paper.questions!) {
        final questionPath =
            questionRF(paperId: paper.id, questionId: questions.id);
        batch.set(questionPath, {
          "question": questions.question,
          "correct_answer": questions.correctAnswer,
        });

        for (var answers in questions.answers) {
          batch
              .set(questionPath.collection("answers").doc(answers.identifier), {
            "identifier": answers.identifier,
            "answer": answers.answer,
          });
        }
      }
    }

    await batch.commit();
    loadingStatus.value = LoadingStatus.completed;
  }
}
