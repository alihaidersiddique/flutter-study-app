import 'package:flutter/material.dart';
import 'package:flutter_study_app/configs/themes/ui_parameters.dart';

const Color onSurfaceTextColor = Colors.white;
const Color cardColor = Color.fromARGB(255, 254, 254, 255);

// colors for light theme
const Color primaryLightColorLight = Color(0xFF3ac3cb);
const Color primaryColorLight = Color(0xFFf85187);
const Color mainTextColorLight = Color.fromARGB(255, 40, 40, 40);
const mainGradientLight = LinearGradient(
  begin: Alignment.topLeft,
  end: Alignment.bottomRight,
  colors: [primaryLightColorLight, primaryColorLight],
);

// colors for dark theme
const Color primaryDarkColorDark = Color(0xFF2e3c62);
const Color primaryColorDark = Color(0xFF99ace1);
const Color mainTextColorDark = Colors.white;
const mainGradientDark = LinearGradient(
  begin: Alignment.topLeft,
  end: Alignment.bottomRight,
  colors: [primaryDarkColorDark, primaryColorDark],
);

LinearGradient mainGradient(BuildContext context) =>
    UIParameters.isDarkMode() ? mainGradientDark : mainGradientLight;

Color customScaffoldColor = UIParameters.isDarkMode()
    ? const Color(0XFF2e3c62)
    : const Color.fromARGB(255, 240, 237, 255);
