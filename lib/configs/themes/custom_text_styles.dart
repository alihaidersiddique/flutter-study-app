import 'package:flutter/material.dart';
import 'package:flutter_study_app/configs/themes/app_colors.dart';
import 'package:flutter_study_app/configs/themes/ui_parameters.dart';
import 'package:get/get.dart';

TextStyle cartTitles = TextStyle(
  color: UIParameters.isDarkMode()
      ? Get.theme.textTheme.bodyText1!.color
      : Get.theme.primaryColor,
  fontSize: 18,
  fontWeight: FontWeight.bold,
);

TextStyle detailText = const TextStyle(fontSize: 12.0);

TextStyle headerText = const TextStyle(
  fontSize: 22.0,
  fontWeight: FontWeight.w700,
  color: onSurfaceTextColor,
);
